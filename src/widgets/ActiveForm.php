<?php

namespace otsec\flatlab\widgets;

class ActiveForm extends \yii\widgets\ActiveForm
{
    /**
     * @inheritdoc
     */
    public $options = [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data', // @fixMe, you`re lazy son of a bitch...
    ];

    /**
     * @inheritdoc
     */
    public $fieldConfig = [
        'template' => "{label}\n<div class=\"col-lg-10\">{input} {error}</div>",
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ];
}