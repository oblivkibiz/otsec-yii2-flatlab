<?php

namespace otsec\flatlab\widgets;

class MainMenu extends \yii\widgets\Menu
{
    /**
     * @inheritdoc
     */
    public $options = [
        'class' => 'sidebar-menu',
        'id' => 'nav-accordion'
    ];

    /**
     * @inheritdoc
     */
    public $itemOptions = [
        'class' => 'sub-menu'
    ];

    /**
     * @inheritdoc
     */
    public $submenuTemplate = "\n<ul class='sub'>\n{items}\n</ul>\n";
}