<?php

namespace otsec\flatlab\assets;

class AdminAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@vendor/otsec/yii2-flatlab/theme/admin';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/bootstrap-reset.css',
        'css/style.css',
        'css/style-responsive.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [

    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}