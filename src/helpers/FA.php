<?php

namespace otsec\flatlab\helpers;

class FA extends \rmrevin\yii\fontawesome\FA
{
    public static function iconText($icon, $text, $options = [])
    {
        return static::icon($icon, $options) . ' ' . $text;
    }
}